#include <Arduino.h>

// Define the DEBUG_MODE macro if you want output to be sent to serial console.
#define DEBUG_MODE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h> // https://github.com/marcoschwartz/LiquidCrystal_I2C

#include <DHT.h>               // https://github.com/adafruit/DHT-sensor-library

#include <PubSubClient.h>      // https://github.com/knolleary/pubsubclient

#include <Time.h>              // https://www.pjrc.com/teensy/td_libs_Time.html
#include <Timezone.h>          // https://github.com/JChristensen/Timezone


// LCD 2004 Definition, I2C address 0x3F
#define LCD_COLS 20
#define LCD_ROWS 4
LiquidCrystal_I2C lcd(0x3F, LCD_COLS, LCD_ROWS);

// Definitions for the two DHT sensors
#define DHT_PIN_1 2
#define DHT_PIN_2 3
#define DHT_TYPE DHT22
DHT dht1(DHT_PIN_1, DHT_TYPE);
DHT dht2(DHT_PIN_2, DHT_TYPE);

// Photocell settings
#define PHOTOCELL_PIN A0
#define LIGHT_TRESHHOLD 700

// Central European Time Zone (Europe/Prague)
// Standard time: UTC+1, DST: UTC+2
const TimeChangeRule mySTD = {"CET", Last, Sun, Oct, 2, +60};
const TimeChangeRule myDST = {"CEST", Last, Sun, Mar, 2, +120};
Timezone myTZ(myDST, mySTD);

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };

// Ethernet client
EthernetClient ethClient;

// A UDP instance to let us send and receive packets over UDP
EthernetUDP Udp;
#define localUDPPort 8888  // local port to listen for UDP packets
IPAddress timeServer(147, 251, 48, 140); // 0.cz.pool.ntp.org

// MQTT PubSubClient
byte MQTT_SERVER[] = { 192, 168, 88, 6 };  // mqtt.n137.fiser.cz
#define MQTT_PORT 1883
PubSubClient mqttClient(ethClient);

// strings for outputs
char strOut[LCD_COLS + 1] = "";
char strTemp[5] = "";
char strHumidity[5] = "";


/*-------- NTP code ----------*/
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}


time_t getNtpTime()
{
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  sendNTPpacket(timeServer);
#ifdef DEBUG_MODE
  Serial.println(F("Sent NTP request."));
#endif

  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
#ifdef DEBUG_MODE
      Serial.println(F("Received NTP response."));
#endif
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL;
    }
  }
#ifdef DEBUG_MODE
  Serial.println(F("No NTP response."));
#endif
  return 0;
}
/*----- End of NTP code -------*/


void publish2MQTT(const char *topic, const char *ts, const char *payload) {

  while (!mqttClient.connected()) {
    //mqttClient.connect("clientID", "mqtt_username", "mqtt_password");
    mqttClient.connect("arduino.sklep");
    mqttClient.publish("n137/arduino.sklep/alive", "I'm alive!");
#ifdef DEBUG_MODE
    Serial.println(F("Successfully connected to MQTT broker."));
#endif
  }

  char message[MQTT_MAX_PACKET_SIZE] = "";
  sprintf(message, "{\"updated\": \"%s\", \"value\": \"%s\"}", ts, payload);
  if (mqttClient.publish(topic, message, true)) {
#ifdef DEBUG_MODE
  Serial.print(F("Data sent to MQTT broker: ["));
  Serial.print(topic);
  Serial.print(F("]: "));
  Serial.println(message);
#endif
    ;
  }
  else {
#ifdef DEBUG_MODE
  Serial.println(F("Failed to publish to MQTT broker"));
#endif
    ;
  }
}


void processDHTData(DHT *dht, const char* ts, const unsigned int lcdLine, const char *mqttTopic) {

  float humidity = dht->readHumidity();
  float tempC = dht->readTemperature();

  if (isnan(humidity) || isnan(tempC)) {
#ifdef DEBUG_MODE
    Serial.println(F("Failed to read DHT sensor!"));
#endif
    ;
  }
  else {
    // avr-libc/Arduino does not support %f in sprintf()
    dtostrf(tempC, 4, 1, strTemp);
    dtostrf(humidity, 4, 1, strHumidity);
    sprintf(strOut, "V: %s C, %s%%", strTemp, strHumidity);

    lcd.setCursor(0, lcdLine);
    lcd.print(strOut);
    lcd.setCursor(7, lcdLine);
    lcd.write(223);  // The degree sign

    publish2MQTT(sprintf("%s/temperature", mqttTopic), ts, strTemp);
    publish2MQTT(sprintf("%s/humidity", mqttTopic), ts, strHumidity);
  }
}


void setup()   {
#ifdef DEBUG_MODE
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
#endif

  // init DHT sensors
  dht1.begin();
  dht2.begin();

  // init the LCD display
  lcd.begin(LCD_COLS, LCD_ROWS);
  lcd.init();
  lcd.backlight();
  lcd.print(F("Starting..."));

  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
#ifdef DEBUG_MODE
    Serial.println(F("Failed to configure Ethernet using DHCP"));
#endif
    // no point in carrying on, so do nothing forevermore:
    lcd.setCursor(0, 0);
  	lcd.print(F("Failed to get IP"));
    for (;;)
      ;
  }
  else {
#ifdef DEBUG_MODE
    Serial.print(F("My IP address: "));
    for (byte thisByte = 0; thisByte < 4; thisByte++) {
      // print the value of each byte of the IP address:
      Serial.print(Ethernet.localIP()[thisByte], DEC);
      Serial.print(F("."));
    }
    Serial.println();
#endif

    // Initiate UDP and set clock sync to use NTP
    Udp.begin(localUDPPort);
    setSyncProvider(getNtpTime);
    setSyncInterval(60 * 30);  // sync time with NTP every 30 minutes
  }

  // Initiate MQTT client
  mqttClient.setServer(MQTT_SERVER, MQTT_PORT);
}


void loop() {

  // Friendly format and display current time
  time_t ts_now = myTZ.toLocal(now());
  sprintf(strOut, "%04d-%02d-%02d %2d:%02d",
		year(ts_now), month(ts_now), day(ts_now), hour(ts_now), minute(ts_now));
  lcd.clear();
  lcd.setCursor(2, 0);
  lcd.print(strOut);

  // Get current time formatted as ISO datetime
  char* ts = myTZ.getLocalISOTime(now());

  // Read values from first DHT22 sensor
  float humidity = dht1.readHumidity();
  float tempC = dht1.readTemperature();
  if (isnan(humidity) || isnan(tempC)) {
#ifdef DEBUG_MODE
    Serial.println(F("Failed to read DHT sensor #1!"));
#endif
    ;
  }
  else {
    // avr-libc/Arduino does not support %f in sprintf()
    dtostrf(tempC, 4, 1, strTemp);
    dtostrf(humidity, 4, 1, strHumidity);
    sprintf(strOut, "V: %s C, %s%%", strTemp, strHumidity);

    lcd.setCursor(0, 1);
    lcd.print(strOut);
    lcd.setCursor(7, 1);
    lcd.write(223);  // The degree sign

    publish2MQTT("n137/sklep/01/temperature", ts, strTemp);
    publish2MQTT("n137/sklep/01/humidity", ts, strHumidity);
  }

  //processDHTData(&dht2, ts, 2, "n137/sklep/02");

  // Read values from second DHT22 sensor
  humidity = dht2.readHumidity();
  tempC = dht2.readTemperature();
  if (isnan(humidity) || isnan(tempC)) {
#ifdef DEBUG_MODE
    Serial.println(F("Failed to read DHT sensor #2!"));
#endif
    ;
  }
  else {
    // avr-libc/Arduino does not support %f in sprintf()
    dtostrf(tempC, 4, 1, strTemp);
    dtostrf(humidity, 4, 1, strHumidity);
    sprintf(strOut, "m: %s C, %s%%", strTemp, strHumidity);

    lcd.setCursor(0, 2);
    lcd.print(strOut);
    lcd.setCursor(7, 2);
    lcd.write(223);  // The degree sign

    publish2MQTT("n137/sklep/02/temperature", ts, strTemp);
    publish2MQTT("n137/sklep/02/humidity", ts, strHumidity);
  }


  // Read value from the fotocell
  unsigned int light = 1023 - analogRead(PHOTOCELL_PIN);
  sprintf(strOut, "photocell: %4d/1023", light);
  lcd.setCursor(0, 3);
  lcd.print(strOut);
  publish2MQTT("n137/sklep/light", ts, (light < LIGHT_TRESHHOLD) ? "off": "on");
#ifdef DEBUG_MODE
  Serial.println(light);
#endif

  // wait -- short wait for DEBUG_MODE mode, longer wait for production
#ifdef DEBUG_MODE
  delay(10000);
#endif
#ifndef DEBUG_MODE
  delay(30000);
#endif
}
